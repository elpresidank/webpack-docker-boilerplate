FROM node:10.15.3-alpine
MAINTAINER Benjamin T Oppold <admin@codedank.com>

RUN npm install -g webpack webpack-dev-server webpack-cli yarn

WORKDIR /app
COPY package.json ./
RUN yarn --pure-lockfile
COPY . ./
RUN yarn build
CMD ["yarn", "start"]

